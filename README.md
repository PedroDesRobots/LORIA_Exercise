
# Open-loop Central Pattern Generator

This project implements an open-loop CPG based on the paper :

Central Pattern Generator with Inertial Feedback for Stable Locomotion and Climbing in Unstructured Terrain
2018 IEEE International Conference on Robotics and Automation (ICRA)
May 21-25, 2018, Brisbane, Australia
Guillaume Sartoretti , Samuel Shaw , Katie Lam , Naixin Fan , Matthew Travers , and Howie Choset.

## Getting Started

```
$ git clone https://PierreDesreumaux@bitbucket.org/PierreDesreumaux/loria_exercise.git
```

You will need to run this command on every new shell you open to have access to the ROS commands
```
$ source /opt/ros/kinetic/setup.bash 
```
Go to catkin_ws
```
$ cd ~/loria_exercise/catkin_ws
```
You need to build your code, make sure there are no errors when it's finish
```
$ catkin_make
```
You can directly launch node
```
$ roslaunch beginner_tutorials  exercise.launch
```
It will launch 2 nodes : JointPositon & CPGVIsualization

JointPosition : compute  joint position during simulation
CPGVisualization : plot a x y graph position.

You can set directly "TimeStep" et "TimeDisplay" paramaters by typing :
```
$ rosparam set "TimeStep" value
$ rosparam set "TimeDisplay" value
```
You can visualize data
```
$ rosrun rviz rviz &
```
![Alt text](https://bitbucket.org/PierreDesreumaux/loria_exercise/src/master/catkin_ws/src/beginner_tutorials/pictures/xy_CPG.png)
![Alt text](https://bitbucket.org/PierreDesreumaux/loria_exercise/src/master/catkin_ws/src/beginner_tutorials/pictures/rqt_graph.png)
you can find pictures here: loria_exercise/catkin_ws/src/beginner_tutorials/pictures

## How write feedback loop CPG ?
I will modify current class "CPG" to add offset (cx and cy).

Then, I will create a new class call CPGOffset which compute the error.
For this class (CPGOffset) we certainly need to add some librabries like for example to compute pseudo-inverse Jacobian :
Kinematics and Dynamics (KDL) library of the Open Robotics Control Software (OROCOS) could be a solution.

Furthermore, we need a model of the hexapod robot to simulate and configure your CPG. We can use gazebo plateform with SimBody simulator which is
based on C++ language.

## Authors

* **Pierre Desreumaux**
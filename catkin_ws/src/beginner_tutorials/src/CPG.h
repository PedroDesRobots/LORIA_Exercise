#pragma once

#ifndef CPG_H_
#define CPG_H_


  class CPG
  {
    public:
	
		CPG();


		void init(double x[6], double y[6],
				double a, double b, double d,
                double lambda, double gamma,
                double w, double T,
                int K[36]);
		
		void setPeriod( double T);
		void setA(double a);
		void setB(double b);
		void setD(double d);
		void setLambda(double lambda);
		void setGamma(double gamma);
		void setW(double w);
		void setK(int K[36]);
		
		double getA();
		double getB();
		double getD();
		double getLambda();
		double getGamma();
		double getW();
		double getT();
		int getK(int i);
		double getX(int i);
		double getY(int i);
		
		void computeCPG();
		
    private:
		
      
      double x_[6];
      double y_[6];
      double a_;
      double b_;
      double d_;
      double lambda_;
      double gamma_;
	  double w_;
	  double T_;
	  int K_[36];
	};
#endif

#include "CPG.h"
#include <cmath>
#include <iostream>
#include <vector>
#include <iterator>

CPG::CPG()
		:a_(1.)
		,b_(1.)
		,d_(1.)
		,lambda_(1.)
		,gamma_(1.)
		,w_(1.)
		,T_(1.)
		{
			int size = int(*(&x_ + 1) - x_);
			for(int i=0; i < size; i++)
			{
				x_[i] = 0;
				y_[i] = 0;
			}
			for(int i=0; i < size*size; i++)
			{
				K_[i] = 0;
			}
		}
		
void CPG::init(double x[6], double y[6],
				double a, double b, double d,
                double lambda, double gamma,
                double w, double T,
                int K[36])
        {
			int size = int(*(&x_ + 1) - x_);
			
			for (int i=0; i < size; i++)
			{
				x_[i] = x[i];
				y_[i] = y[i];
			}
			a_ = a;
			b_ = b;
			d_ = d;
			lambda_ = lambda;
			gamma_ = gamma;
			w_ = w;
			T_ = T;
			
			int sizeK = int(*(&K_ + 1) - K_);
			for (int i=0; i < sizeK; i++)
			{
				K_[i] = K[i];
			}
			
		}

void CPG::computeCPG()
	{
		int size = int(*(&x_ + 1) - x_);
		
		for (int i=0; i < size; i++)
		{ 
			double Hxy = std::pow((x_[i]/a_),d_) + std::pow((y_[i]/b_),d_); 
			double dHy = (d_/std::pow(b_,d_)) * std::pow(y_[i],(d_-1));
			double dHx = (d_/std::pow(a_,d_)) * std::pow(x_[i],(d_-1));
			double lambda_Ky =0;
		
		std::cout << "dHy : " << dHy << std::endl;
		
			for (int j=0; j < size; j++)
			{
				double Ky = K_[i*size+j]* y_[j];
				lambda_Ky = lambda_Ky + Ky*lambda_;
				std::cout << "Ky " << Ky << std::endl;
			} 
		
			//First order Taylor approximation
			x_[i] = T_ * (-w_ * dHy + gamma_ * (1-Hxy) * dHx) + x_[i];
			y_[i] = T_ * (w_ * dHx + gamma_ * (1-Hxy) * dHy + lambda_Ky) + y_[i]; 
		}	 
	}
	
void CPG::setPeriod( double T)
	{
		T_  = T;
	}
	
void CPG::setA(double a)
	{
		a_  = a;
	}		
void CPG::setB(double b)
	{
		b_  = b;
	}		

void CPG::setD(double d)
	{
		d_  = d;
	}		

void CPG::setLambda(double lambda)
	{
		lambda_  = lambda;
	}		

void CPG::setGamma(double gamma)
	{
		gamma_  = gamma;
	}		

void CPG::setW(double w)
	{
		w_  = w;
	}		

void CPG::setK(int K[36])
	{
		int size = int(*(&x_ + 1) - x_);
		for(int i=0; i <size*size ; i++)
		{
			K_[i] = K[i];
		}
	}		


double CPG::getX(int i)
	{
		return x_[i];
	}
	
double CPG::getY(int i)
	{
		return y_[i];
	}

double CPG::getA()
	{
		return a_;
	}
	
double CPG::getB()
	{
		return b_;
	}
	
double CPG::getD()
	{
		return d_;
	}
	
double CPG::getLambda()
	{
		return lambda_;
	}
	
double CPG::getGamma()
	{
		return gamma_;
	}
	
double CPG::getW()
	{
		return w_;
	}
	
double CPG::getT()
	{
		return T_;
	}
	
int CPG::getK(int i)
	{
		return K_[i];
	}
		

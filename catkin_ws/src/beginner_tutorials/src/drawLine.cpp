#include <ros/ros.h>
#include <visualization_msgs/Marker.h>
#include <beginner_tutorials/jointPosition.h>
#include <math.h>

class DrawLine
{
public:
  DrawLine()
  {
	pub_ = n_.advertise<visualization_msgs::Marker>("CPGvisualization", 1);
   
    sub_ = n_.subscribe("jointPosition",1,&DrawLine::publishMsg_callback, this);
  }

  void publishMsg_callback(const beginner_tutorials::jointPosition& subMsg)
  {
		line_strip.header.frame_id = "/my_frame";
		line_strip.header.stamp = ros::Time::now();
		line_strip.ns = "points_and_lines";
		line_strip.action = visualization_msgs::Marker::ADD;
		line_strip.pose.orientation.w = 1.0;
		
        line_strip.id = 0;
  
        line_strip.type = visualization_msgs::Marker::LINE_STRIP;
   
        line_strip.color.b = 1.0;
        line_strip.color.a = 1.0;
		
        line_strip.scale.x = 0.1;
        
        geometry_msgs::Point p;
        p.x = subMsg.x[0];
        p.y = subMsg.y[0];
        p.z = 0;
		std::cout <<"px : "<< p.x << std::endl;
        line_strip.points.push_back(p);
		
        pub_.publish(line_strip);
  }

private:
  ros::NodeHandle n_; 
  ros::Publisher pub_;
  ros::Subscriber sub_;
  visualization_msgs::Marker points, line_strip;
};

int main(int argc, char **argv)
{
  ros::init(argc, argv, "CPGvisualization");
  DrawLine myDrawLine;
  ros::spin();

  return 0;
}

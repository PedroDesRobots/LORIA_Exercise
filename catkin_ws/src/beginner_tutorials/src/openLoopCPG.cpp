
#include "ros/ros.h"
#include "CPG.h"

#include <beginner_tutorials/jointPosition.h>
#include <sstream>
#include <math.h>


int main(int argc, char **argv)
{
	ros::init(argc, argv,"jointPosition");	 
	ros::NodeHandle n;
	ros::Publisher jointPosition_Publisher = n.advertise<beginner_tutorials::jointPosition>("jointPosition", 10);
	 
	double Td = 2;
	double pi =  3.1416;
	double x[6] = {5,1,1,1,1,1};
	double y[6] =  {1,1,1,1,1,1};// {0.1,0.1,0.1,0.1,0.1,0.1};
	double a = 1;
	double b = 2;
	double d = 2;
	double lambda = 0;
	double  gamma = 2;
	double w = 5;
	double T = 0.01;
	int K[36] = {0,-1, -1, 1, 1, -1,
				 -1, 0, 1, -1, -1, 1,
				 -1, 1, 0, -1, -1, 1,
				 1, -1, -1, 0, 1, -1,
				 1, -1, -1, 1, 0, -1,
				 -1, 1, 1, -1, -1, 0,
				};
	
	int size = int(*(&x+1)-x);
	int counter = 0;

	CPG myCPG;
	myCPG.init(x, y, a, b, d, lambda, gamma, w, T, K);
	
	ros::Rate loop_rate(Td);	 

	 	
	while(ros::ok())
	{
		 if(n.getParam("TimeStep", T))
		 {
			 if(T<1){myCPG.setPeriod(T);}
			 else {std::cout << "not allowed" << std::endl;} 
		 }
		 if(n.getParam("TimeDisplay", Td))
		 {
			ros::Rate loop_rate(Td); 
		 }
		
		 myCPG.computeCPG();
		 
		 beginner_tutorials::jointPosition msg;
		 
		 for(int  i=0; i< size;i++)
		 {
			msg.x[i] = myCPG.getX(i);
			msg.y[i] = myCPG.getY(i);
			std::cout << "x : " << msg.x[i] << "\n" << std::endl;
			std::cout << "y : " << msg.y[i] << "\n" << std::endl;
		 }
		 
		 jointPosition_Publisher.publish(msg);	 
		 
		 ros::spinOnce();
		 
		 loop_rate.sleep();
		 ++counter;
	}
	return 0;
}

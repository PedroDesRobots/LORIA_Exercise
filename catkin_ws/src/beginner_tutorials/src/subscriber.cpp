#include "ros/ros.h"
#include "CPG.h"

#include <beginner_tutorials/jointPosition.h>
#include <sstream>
#include <math.h>

void publishMsg_callback(const beginner_tutorials::jointPosition& subMsg)
{
	std::cout << "Message receive : "<< std::endl;
	
	int size = 6;
	for(int i = 0; i < size; i++)
	{
		std::cout <<"x["<< i <<"]: " << subMsg.x[i] << std::endl;
		std::cout <<"y["<< i <<"]: " << subMsg.y[i] << std::endl;		
	} 
	std::cout << "End message" << std::endl; 
}

int main(int argc, char **argv)
{
	ros::init(argc, argv,"subscriberMessage");	 
	ros::NodeHandle n;
	ros::Subscriber jointPosition_Subscriber = n.subscribe("jointPosition",10,publishMsg_callback);
	ros::spin();
	
	return 0;
}
	 

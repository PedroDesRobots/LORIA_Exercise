
(cl:in-package :asdf)

(defsystem "beginner_tutorials-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils )
  :components ((:file "_package")
    (:file "jointPosition" :depends-on ("_package_jointPosition"))
    (:file "_package_jointPosition" :depends-on ("_package"))
  ))